import Vue from 'vue'

export default {
    save(photo) {
        return Vue.axios.post('http://localhost/Vue.js/pruebaConocimiento%202/php/processImage.php?action=save', photo);
    },

    delete(namePhoto) {
        return Vue.axios.post('http://localhost/Vue.js/pruebaConocimiento%202/php/processImage.php?action=delete', namePhoto);
    }
}