<?php
    $action = '';
    $route = $_SERVER['DOCUMENT_ROOT'].'/Vue.js/pruebaConocimiento 2/crud-productos-2/src/assets/images/';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    switch($action) {
        case 'save':
            if (isset($_FILES['photo'])) {
                $namePhoto = time().$_FILES['photo']['name'];  
                move_uploaded_file($_FILES['photo']['tmp_name'],$route.$namePhoto);
                $response = $namePhoto;
            } else {
                $response['error'] = true;
                $response['message'] = 'Foto guardada exitosamente!';
            }

            echo json_encode($response);
            break;

        case 'delete':
            if (isset($_POST['namePhoto'])) {
                $namePhoto = $_POST['namePhoto'];
                if (unlink($route.$namePhoto)) {
                    $response['message'] = 'Delete photo success';
                } else {
                    $response['error'] = true;
                    $response['message'] = 'No se pudo borrar la foto!';
                }
            }

            echo json_encode($response);
            break;
    }
    
?>